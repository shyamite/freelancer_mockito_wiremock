package com.krish.freeme.service;

import com.krish.freeme.dto.CategoryDTO;
import com.krish.freeme.entity.Category;
import com.krish.freeme.repository.ICategoryRepo;
import com.krish.freeme.service.interfaces.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class CategoryService implements ICategoryService{

    @Autowired
    private ICategoryRepo iCategoryRepo;

    @Override
    public CategoryDTO createCategory(CategoryDTO categoryDTO) {
        Category category = iCategoryRepo.save(new Category(categoryDTO));
        return new CategoryDTO(category);
    }

    @Override
    public CategoryDTO getCategory(CategoryDTO categoryDTO) {
        return null;
    }
}
