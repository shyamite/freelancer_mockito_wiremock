package com.krish.freeme.service.interfaces;

import com.krish.freeme.dto.CategoryDTO;

public interface ICategoryService {
    CategoryDTO createCategory(CategoryDTO categoryDTO);
    CategoryDTO getCategory(CategoryDTO categoryDTO);
}
