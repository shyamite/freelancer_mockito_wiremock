package com.krish.freeme.dto;

import com.krish.freeme.entity.Category;
import com.krish.freeme.enums.LevelOfSkill;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;

public class CategoryDTO {

    private String name;
    private String description;
    private String categoryref ;
    private LevelOfSkill levelOfSkills;

    public CategoryDTO(Category category) {
        this.name = category.getName();
        this.description = category.getDescription();
        this.categoryref = category.getCategoryref();
        this.levelOfSkills = category.getLevelOfSkills();
    }

    public CategoryDTO() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategoryref() {
        return categoryref;
    }

    public void setCategoryRef(String categoryref) {
        this.categoryref = categoryref;
    }

    public LevelOfSkill getLevelOfSkills() {
        return levelOfSkills;
    }

    public void setLevelOfSkills(LevelOfSkill levelOfSkills) {
        this.levelOfSkills = levelOfSkills;
    }

    @Override
    public String toString() {
        return "CategoryDTO{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", categoryRef='" + categoryref + '\'' +
                ", levelOfSkills=" + levelOfSkills +
                '}';
    }
}
