package com.krish.freeme.enums;

import java.util.Set;
import com.google.common.collect.Sets;
/**
 * Created by Krishan Shukla
 */

public enum LevelOfSkill {

    NON_SKILL,
    MEDIUM_SKILL,
    HIGH_SKILL,
    UNIQUE_SKILL;

    public static Set<LevelOfSkill> getAllSkills() {
        return Sets.newHashSet(NON_SKILL, MEDIUM_SKILL, HIGH_SKILL,UNIQUE_SKILL);
    }
}
