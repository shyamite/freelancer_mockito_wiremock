package com.krish.freeme.controller;

import com.krish.freeme.dto.CategoryDTO;
import com.krish.freeme.service.interfaces.ICategoryService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/category")
public class CategoryController {

    @Autowired
    private ICategoryService categoryService;

    private static final Logger logger = Logger.getLogger(CategoryController.class);
///v1/category/createcategory
    @RequestMapping(value = "/createcategory", method = RequestMethod.POST)
    public CategoryDTO createCategory(@RequestBody CategoryDTO categoryDTO) {
        logger.info("++createcategory");
        System.out.println("incoming dto" + categoryDTO);
        CategoryDTO category = categoryService.createCategory(categoryDTO);
        System.out.println("outgoing dto" + category);

        logger.info("--createcategory");
        return category;
    }

    @RequestMapping(value = "/onecategory", method = RequestMethod.POST)
    public CategoryDTO oneCategory(@RequestBody CategoryDTO categoryDTO) {
        logger.info("++onecategory");
        CategoryDTO category = categoryService.getCategory(categoryDTO);
        logger.info("--onecategory");
        return category;
    }


}
