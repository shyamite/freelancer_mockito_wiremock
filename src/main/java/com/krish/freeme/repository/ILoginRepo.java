package com.krish.freeme.repository;

import com.krish.freeme.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface ILoginRepo extends CrudRepository<User, Long>{

    public User findByEmailId(String emailId);


}