package com.krish.freeme.repository;

import com.krish.freeme.entity.Category;
import com.krish.freeme.enums.LevelOfSkill;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface ICategoryRepo extends CrudRepository<Category, Long> {

    public Category findByName(String name);
    public Category findByCategoryref(String categoryRef);
    public List<Category> findByLevelOfSkills(LevelOfSkill levelOfSkills);

    public Category save(Category category);

}
