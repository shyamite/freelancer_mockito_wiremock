package com.krish.freeme.entity;

import com.krish.freeme.dto.AddressDTO;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.GregorianCalendar;

@Entity
public class Address implements Serializable{
    private static final long serialVersionUID = 1L;

    @javax.persistence.Id
    @GeneratedValue
    private Long Id;

    //Address properties
    private String houseNumber;
    private String firstLine;
    private String secondLine;
    private String city;
    private String postCode;
    private String country;
    private String state;



    public Address(AddressDTO addressDTO)
    {
        this.houseNumber =addressDTO.getHouseNumber();
        this.firstLine =addressDTO.getFirstLine();
        this.secondLine=addressDTO.getSecondLine();
        this.city=addressDTO.getCity();
        this.postCode=addressDTO.getPostCode();
        this.country=addressDTO.getCountry();
    }
    public Address()
    {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @ManyToOne
    @JoinColumn(name="userId" , nullable=false)
    private User user;


    @Column(nullable = false)
    @CreationTimestamp
    private Timestamp createTime = new Timestamp(GregorianCalendar.getInstance().getTimeInMillis());;

    @Column(nullable = false)
    @UpdateTimestamp
    private Timestamp editTime = new Timestamp(GregorianCalendar.getInstance().getTimeInMillis());

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }


}
