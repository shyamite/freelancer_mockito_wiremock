package com.krish.freeme.entity;

import com.krish.freeme.dto.CategoryDTO;
import com.krish.freeme.enums.LevelOfSkill;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;

@Entity
public class Category {

    @javax.persistence.Id
    @GeneratedValue
    private Long Id;

    private String name;
    private String description;
    private String categoryref ;

    @Enumerated(EnumType.STRING)
    private LevelOfSkill levelOfSkills;

    public Category(CategoryDTO categoryDTO){
        this.name =categoryDTO.getName();
        this.description = categoryDTO.getDescription();
        this.categoryref = categoryDTO.getCategoryref();
        this.levelOfSkills = categoryDTO.getLevelOfSkills();
    }


    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategoryref() {
        return categoryref;
    }

    public void setCategoryref(String categoryref) {
        this.categoryref = categoryref;
    }

    public LevelOfSkill getLevelOfSkills() {
        return levelOfSkills;
    }

    public void setLevelOfSkills(LevelOfSkill levelOfSkills) {
        this.levelOfSkills = levelOfSkills;
    }

    @Override
    public String toString() {
        return "Category{" +
                "Id=" + Id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", categoryref='" + categoryref + '\'' +
                ", levelOfSkills=" + levelOfSkills +
                '}';
    }
}
