package com.krish.freeme;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages={"com.krish.*"})
public class FreelancerMainApp {

    public static void main(String[] args) {
        SpringApplication.run(FreelancerMainApp.class , args);
    }
   
}
