package com.krish.freeme.service;

import com.krish.freeme.controller.CategoryController;
import com.krish.freeme.dto.CategoryDTO;
import com.krish.freeme.entity.Category;
import com.krish.freeme.enums.LevelOfSkill;
import com.krish.freeme.repository.ICategoryRepo;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

import static org.mockito.Mockito.*;


/**
 * @author Krishan Shukla
 * @Date 28/01/2018
 */

public class CategoryServiceTest {

    @InjectMocks
    private CategoryService categoryService;

   @Mock
   private ICategoryRepo iCategoryRepo;

    private CategoryDTO categoryDTO;
    private CategoryDTO categoryDTO2;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
       // categoryDTO = mock(CategoryDTO.class);
        categoryDTO = new CategoryDTO();

      //  categoryDTO2 = mock(CategoryDTO.class);
        categoryDTO2 = new CategoryDTO();

        // categoryDTO = new CategoryDTO();
        categoryDTO.setName("Student");
        categoryDTO.setDescription("This cat is using for education");
        categoryDTO.setCategoryRef("A");
        categoryDTO.setLevelOfSkills(LevelOfSkill.UNIQUE_SKILL);

        categoryDTO2.setName("Chairman");
        categoryDTO2.setDescription("mouse");
        categoryDTO2.setCategoryRef("C");
        categoryDTO2.setLevelOfSkills(LevelOfSkill.UNIQUE_SKILL);
    }
    @Test
    public void createCategoryTest() {
       /* System.out.println("++createCategoryTest with categoryControllerInstance : " + categoryController);
        System.out.println("++createCategoryTest with categoryServiceInstance : " + categoryService);*/
        System.out.println("in" + categoryDTO);
        categoryDTO.setName("Testteacher");
        categoryDTO.setDescription("This bat is using for education");
        categoryDTO.setCategoryRef("B");
        categoryDTO.setLevelOfSkills(LevelOfSkill.UNIQUE_SKILL);


      // when(iCategoryRepo.save(new Category(categoryDTO))).thenReturn(new Category(categoryDTO));
        Category cat =new Category(categoryDTO);
        //when(iCategoryRepo.save(cat)).thenReturn(cat);
        when(iCategoryRepo.save(any(Category.class))).thenReturn(cat);

        CategoryDTO categoryDTO34 = categoryService.createCategory(categoryDTO);
        System.out.println("RRRRRRoo==="+cat);
        System.out.println("RRRRRR==="+categoryDTO34);
      //  CategoryDTO categoryDTO34 = categoryService.getCat1(cat);
      //  System.out.println("RRRRRR666==="+categoryDTO34);
       // CategoryDTO testCategoryDTO1 = categoryService.createCategory(categoryDTO);
      //  Assert.assertEquals(categoryService.createCategory(categoryDTO), categoryDTO);
       // System.out.println("testCategoryDTO1" + testCategoryDTO1 );

    }

    @After
    public void end() {
        System.out.println("ending tests");

    }

}
