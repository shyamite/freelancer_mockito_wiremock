package com.krish.freeme.controller;

import com.krish.freeme.controller.CategoryController;
import com.krish.freeme.dto.CategoryDTO;
import com.krish.freeme.entity.Category;
import com.krish.freeme.enums.LevelOfSkill;
import com.krish.freeme.repository.ICategoryRepo;
import com.krish.freeme.service.CategoryService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;

import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Krishan Shukla
 * @Date 28/01/2018
 */

public class CategoryControllerTest_old {
    @InjectMocks
    private CategoryController categoryController;
    @Mock
    private CategoryService categoryService;

    private CategoryDTO categoryDTO;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        categoryDTO = mock(CategoryDTO.class);
        // categoryDTO = new CategoryDTO();
        categoryDTO.setName("Student");
        categoryDTO.setDescription("This cat is using for education");
        categoryDTO.setCategoryRef("A");
        categoryDTO.setLevelOfSkills(LevelOfSkill.UNIQUE_SKILL);
    }
    @Test
    public void createCategoryTest() {
       /* System.out.println("++createCategoryTest with categoryControllerInstance : " + categoryController);
        System.out.println("++createCategoryTest with categoryServiceInstance : " + categoryService);*/
        System.out.println("in" + categoryDTO);
        categoryDTO.setName("teacher");

        when(categoryService.createCategory(categoryDTO))
                .thenReturn(categoryDTO);
        CategoryDTO testCategoryDTO1 = categoryController.createCategory(categoryDTO);

    }

    @After
    public void end() {
        System.out.println("ending tests");

    }

}
