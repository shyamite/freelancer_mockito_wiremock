package com.krish.freeme.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.krish.freeme.dto.CategoryDTO;
import com.krish.freeme.entity.Category;
import com.krish.freeme.enums.LevelOfSkill;
import com.krish.freeme.service.interfaces.ICategoryService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Krishan Shukla
 * @Date 30/01/2018
 */
@RunWith(SpringRunner.class)
@WebMvcTest(CategoryController.class)
public class CategoryControllerTest {

    private static final String CATEGORY_NAME = "TUTION";
    private static final String CATEGORY_DESC = "DO YOU WANT TO TEACH KIDS";
    private static final String CATEGORY_REF = "mycatref";

   /* private static final long DIGITAL_SERVICE_KEY = 111222333L;
    private static final String TRANSACTION_DATE_STRING = "2018-01-13T00:00:00.000+0000";
    private static final Date TRANSACTION_DATE = StringToDate(TRANSACTION_DATE_STRING);
    private static final String EMPTY_JSON = "{}";
    private static final String TEST_RESULT_MESSAGE = "test message";*/


    /*@InjectMocks
    private CategoryController categoryController;*/
    @Autowired
    private MockMvc mvc;
    @MockBean
    private ICategoryService categoryService;

    private CategoryDTO categoryDTO;
    private Category category;

    @Mock
    private CategoryDTO categoryDTO2;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        categoryDTO = mock(CategoryDTO.class);
        categoryDTO.setName(CATEGORY_NAME);
        categoryDTO.setDescription(CATEGORY_DESC);
        categoryDTO.setCategoryRef(CATEGORY_REF);
        categoryDTO.setLevelOfSkills(LevelOfSkill.UNIQUE_SKILL);

        category = mock(Category.class);

        category.setName("Padai");
        category.setDescription("aise");
        category.setCategoryref(CATEGORY_REF);
        category.setLevelOfSkills(LevelOfSkill.UNIQUE_SKILL);

    }


    @Test
    public void createCategoryTest() throws Exception {

        when(categoryService.createCategory(categoryDTO)).thenReturn(categoryDTO2);
        categoryDTO2 = categoryService.createCategory(categoryDTO);
        categoryDTO2.setName("Home Cleaning");


        MockHttpServletResponse response = mvc.perform(post("/v1/category/createcategory")
                .content(buildComputeInterestDeltaRequest())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse();

        assertThat(response.getContentAsString(), is(category.getName()));


    }

    private String buildComputeInterestDeltaRequest() throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(
                new CategoryDTO(category));
    }


}
